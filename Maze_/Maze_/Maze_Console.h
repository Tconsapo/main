#ifndef MAZE_CONSOLE_H
#define MAZE_CONSOLE_H

#include <iostream>
#include <windows.h>
#include "Maze.h"

using namespace std;

class Maze_Console
{
public:
	class Maze GMaze_out(const int &mx, const int &my); //����� + ���������
	class Maze start(); //����� ����
	void out_maze(class Maze &mz); //������� �����
	Maze_Console();
	~Maze_Console();
};

#endif