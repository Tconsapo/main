#ifndef MAZE_H
#define MAZE_H

#include <iostream>
#include<random>
#include<time.h>
#include"Player.h"
#include<string>

struct T //������ ���������
{
	bool d_wall = false; //������ �����
	bool r_wall = false; //������ �����
	bool out = false; //�����
	bool player = false; //�����
};

class Maze
{
	T** Maze_;	//���� ���������
	int Height; //������
	int Width;	//������
	int xs, ys, xf, yf; //�����,�����
	std::string way; //���� �� ������� ������

public:
	Maze(const int &h, const int &w); 
	friend class Player;
	friend class Maze_Console;
	void Generate(); //���������
	friend std::ostream &operator << (std::ostream &out, const Maze &x);
	friend bool fnd(const int &y, const int &x, bool** b, const Maze &m); //���� �� ����� ������������ ������
	friend void Way(const int &y, const int &x, bool** _Be, Maze &m); //���������
	friend void Bot_Way(const class Maze &m, const int &start_y, const int &start_x, bool** be, std::string** ways); //���� ����
	friend void Bot_Start(class Maze &m, const int &start_y, const int &start_x); //����� ����
	Maze operator = (const Maze &x);
	Maze(const Maze &x);
	~Maze();
};

#endif