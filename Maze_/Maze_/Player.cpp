#include "Player.h"

void Player::Bot_Way(class Maze &m, const int &start_y, const int &start_x, bool** &be, std::string** &ways) //����� ���� � �������
{
	be[start_y][start_x] = true;
	if ((m.xf != start_x) || (m.yf != start_y))
	{
		if (start_y - 1 > 0)
		{
			if ((!be[start_y - 1][start_x]) && (!m.Maze_[start_y - 1][start_x].d_wall))
			{
				be[start_y - 1][start_x] = true;
				ways[start_y - 1][start_x] = ways[start_y][start_x] + " u";
				Bot_Way(m, start_y - 1, start_x, be, ways);
			}
			else
			{
				if (!m.Maze_[start_y - 1][start_x].d_wall)
				{
					if (ways[start_y][start_x].length() + 2 < ways[start_y - 1][start_x].length())
					{
						ways[start_y - 1][start_x] = ways[start_y][start_x] + " u";
						Bot_Way(m, start_y - 1, start_x, be, ways);
					}
				}
			}
		}

		if (start_y + 1 < m.Height)
		{
			if ((!be[start_y + 1][start_x]) && (!m.Maze_[start_y][start_x].d_wall))
			{
				be[start_y + 1][start_x] = true;
				ways[start_y + 1][start_x] = ways[start_y][start_x] + " d";
				Bot_Way(m, start_y + 1, start_x, be, ways);
			}
			else
			{
				if (!m.Maze_[start_y][start_x].d_wall)
				{
					if (ways[start_y][start_x].length() + 2 < ways[start_y + 1][start_x].length())
					{
						ways[start_y + 1][start_x] = ways[start_y][start_x] + " d";
						Bot_Way(m, start_y + 1, start_x, be, ways);
					}
				}
			}
		}

		if (start_x - 1 > 0)
		{
			if ((!be[start_y][start_x - 1]) && (!m.Maze_[start_y][start_x - 1].r_wall))
			{
				be[start_y][start_x - 1] = true;
				ways[start_y][start_x - 1] = ways[start_y][start_x] + " l";
				Bot_Way(m, start_y, start_x - 1, be, ways);
			}
			else
			{
				if (!m.Maze_[start_y][start_x - 1].r_wall)
				{
					if (ways[start_y][start_x].length() + 2 < ways[start_y][start_x - 1].length())
					{
						ways[start_y][start_x - 1] = ways[start_y][start_x] + " l";
						Bot_Way(m, start_y, start_x - 1, be, ways);
					}
				}
			}
		}

		if (start_x + 1 < m.Width)
		{
			if ((!be[start_y][start_x + 1]) && (!m.Maze_[start_y][start_x].r_wall))
			{
				be[start_y][start_x + 1] = true;
				ways[start_y][start_x + 1] = ways[start_y][start_x] + " r";
				Bot_Way(m, start_y, start_x + 1, be, ways);
			}
			else
			{
				if (!m.Maze_[start_y][start_x].r_wall)
				{
					if (ways[start_y][start_x].length() + 2 < ways[start_y][start_x + 1].length())
					{
						ways[start_y][start_x + 1] = ways[start_y][start_x] + " l";
						Bot_Way(m, start_y, start_x + 1, be, ways);
					}
				}
			}
		}
	}
}

void Player::Bot_Start(class Maze &m, const int &start_y, const int &start_x) //����� ������ ����
{
	std::string** ways;	//������ �����
	bool** be;	//����� ����������
	ways = new std::string*[m.Height];
	be = new bool*[m.Height];

	for (int i = 0; i < m.Height; i++)
	{
		ways[i] = new std::string[m.Width];
		be[i] = new bool[m.Width];
		for (int j = 0; j < m.Width; j++)
		{
			be[i][j] = false;
			ways[i][j] = "\0";
		}
	}

	Bot_Way(m, start_y, start_x, be, ways);

	m.way = ways[m.yf][m.xf];

	for (int i = 0; i < m.Height; i++)
	{
		delete[] be[i];
		delete[] ways[i];
	}
	delete[] be;
	delete[] ways;
}

int Player::Go(class Maze &m) //�����
{
	x = m.xs;
	y = m.ys;
	Maze_Console out;
	while (!m.Maze_[y][x].out) //���� �� ����� �� ������
	{
		out.out_maze(m);
		int ch = _getch();

		if (ch == 27) {
			break;
		}

		if (ch == 224)
		{
			ch = _getch();
			switch (ch)
			{
			case UP_ARROW:
				if (y - 1 > 0)
				{
					if (!m.Maze_[y - 1][x].d_wall)
					{
						m.Maze_[y - 1][x].player = true;
						m.Maze_[y][x].player = false;
						y = y - 1;
					}
				}
				break;
			case DOWN_ARROW:
				if (y + 1 < m.Height)
				{
					if (!m.Maze_[y][x].d_wall)
					{
						m.Maze_[y][x].player = false;
						m.Maze_[y + 1][x].player = true;
						y = y + 1;
					}
				}
				break;
			case LEFT_ARROW:
				if (x - 1 > 0)
				{
					if (!m.Maze_[y][x - 1].r_wall)
					{
						m.Maze_[y][x - 1].player = true;
						m.Maze_[y][x].player = false;
						x = x - 1;
					}
				}
				break;
			case RIGHT_ARROW:
				if (x + 1 < m.Width)
				{
					if (!m.Maze_[y][x].r_wall)
					{
						m.Maze_[y][x].player = false;
						m.Maze_[y][x + 1].player = true;
						x = x + 1;
					}
				}
				break;
			}
		}

	}
	out.out_maze(m);
	m.Maze_[y][x].player = false; 
	m.Maze_[m.ys][m.xs].player = true; //����������� �� �����
	std::cout << endl;
	std::cout << "���� ���������" << endl;
	std::cout << "��������� ������ - 1;" << endl << "������������� ������ - 2:" << endl;
	std::cout << "��������� - 3;" << endl << "�������� ������ ���� - 4" << endl;
	int u = 0;
	while ((u != 1) && (u != 2) && (u != 3) && (u != 4))
	{
		std::cin >> u;
	}
	return u;
}

std::string Player::Get_way(class Maze &mh, const int &n) //����� ����
{
	Bot_Start(mh, y, x);

	int q = n;
	std::string w;

	if (q > mh.way.length())
		q = mh.way.length() / 2;

	for (int i = 0; i < q * 2; i++)
	{
		w = w + mh.way[i];
	}

	return w;
}

Player::Player(class Maze &m)
{
	x = m.xs;
	y = m.ys;
}

Player::Player()
{
	x = 1;
	y = 1;
}

Player::~Player()
{
}
