#include "main_window.h"


Main_Window::Main_Window(QWidget *parent) : QWidget(parent)
{
    Generate = new QPushButton("Сгенерировать");
    Generate->setMaximumSize(90,30);
    Generate->setMinimumSize(90,30);

    Restart = new QPushButton("Начать сначала");
    Restart->setMaximumSize(90,30);
    Restart->setMinimumSize(90,30);

    Get_Way = new QPushButton("Показать путь");
    Get_Way->setMaximumSize(90,30);
    Get_Way->setMinimumSize(90,30);

    ext = new QPushButton("Выход");
    ext->setMaximumSize(90,30);
    ext->setMinimumSize(90,30);

    height_e = new QTextEdit("15");
    height_e->setMinimumSize(45,30);
    height_e->setMaximumSize(45,30);
    height_e->setTabChangesFocus(true);
    height_e->document()->setMaximumBlockCount(1);

    width_e = new QTextEdit("15");
    width_e->setMinimumSize(45,30);
    width_e->setMaximumSize(45,30);
    width_e->setTabChangesFocus(true);
    width_e->document()->setMaximumBlockCount(1);

    QHBoxLayout *main_layout = new QHBoxLayout;
    QVBoxLayout *vbl_main = new QVBoxLayout;
    QHBoxLayout *hbl_1 = new QHBoxLayout;

    hbl_1->addWidget(height_e);
    hbl_1->addWidget(width_e);
    hbl_1->addWidget(Generate);

    vbl_main->addLayout(hbl_1);
    vbl_main->addWidget(Get_Way);
    vbl_main->addWidget(Restart);
    vbl_main->addWidget(ext);

    vbl_main->setAlignment(hbl_1,Qt::AlignRight);
    vbl_main->setAlignment(Get_Way,Qt::AlignRight);
    vbl_main->setAlignment(Restart,Qt::AlignRight);
    vbl_main->setAlignment(ext,Qt::AlignRight);

    mz = new Maze_Widget(height_e->toPlainText().toInt(),
                         width_e->toPlainText().toInt());

    mz->setMinimumSize(width_e->toPlainText().toInt()*21 + 1,
                        height_e->toPlainText().toInt()*21 + 1);

    main_layout->addWidget(mz);
    main_layout->addLayout(vbl_main);
    main_layout->setAlignment(vbl_main,Qt::AlignTop);


    this->setMinimumSize(width_e->toPlainText().toInt()*21 + 1 + 225,
                         height_e->toPlainText().toInt()*21 + 1 + 10);
    this->setMaximumSize(width_e->toPlainText().toInt()*21 + 1 + 225,
                         height_e->toPlainText().toInt()*21 + 1 + 10);


    connect(Generate,SIGNAL(clicked(bool)),this,SLOT(Generate_mz()));
    connect(this,SIGNAL(Gen_ok(int&,int&)),mz,SLOT(Generate_maze(int&,int&)));
    connect(ext,SIGNAL(clicked(bool)),this,SLOT(close()));
    connect(Restart,SIGNAL(clicked(bool)),mz,SLOT(restart()));
    connect(Get_Way,SIGNAL(clicked(bool)),mz,SLOT(Bot()));

    this->setTabOrder(mz,height_e);
    this->setTabOrder(height_e,width_e);
    this->setTabOrder(width_e,Generate);
    this->setTabOrder(Generate,Get_Way);
    this->setTabOrder(Get_Way,Restart);
    this->setTabOrder(Restart,ext);
    this->setTabOrder(ext,mz);

    this->setLayout(main_layout);
}

void  Main_Window::Generate_mz()
{
    bool b = true;
    int w = width_e->toPlainText().toInt(&b);
    int h = height_e->toPlainText().toInt(&b);
    if (b == true) //проверка условий генерации лабиринта
    {
        if ((w>=8)&&(h>=8)&&(w<=32)&&(h<=32))
        {
            emit Gen_ok(w,h);
            this->setMinimumSize(w*21 + 1 + 225, h*21 + 1 + 10);
            this->setMaximumSize(w*21 + 1 + 225, h*21 + 1 + 10);
        }
        else
        {
            M_Dialog *err = new M_Dialog(QString("Размеры лабиринта: [8..32]"));
            err->show();
            mz->setFocus();
        }
    }
}
