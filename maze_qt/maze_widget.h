//виджет лабиринта

#ifndef MAZE_WIDGET_H
#define MAZE_WIDGET_H

#include <QWidget>
#include "Maze.h"
#include <QPainter>
#include "Player.h"
#include <QKeyEvent>
#include "m_dialog.h"

class QPaintEvent;
class QKeyPressEvent;

class Maze_Widget : public QWidget
{
    Q_OBJECT
    Maze *mzx; //лабиринт
    Player *pl; //игрок

public:
    Maze_Widget(int h /*высота*/, int w /*ширина*/, QWidget *parent = 0);
protected:
    void paintEvent(QPaintEvent *);
    void keyPressEvent(QKeyEvent *pressed);
    void pUp();     //стрелка вверх нажата
    void pDown();   //стрелка вниз нажата
    void pLeft();   //стрелка влево нажата
    void pRight();  //стрелка вправо нажата
signals:

public slots:
    void Generate_maze(int &w, int &h); //Создание
    void restart(); //начать снова
    void Bot(); //путь
};

#endif // MAZE_WIDGET_H
