//Окно сообщения

#ifndef M_DIALOG_H
#define M_DIALOG_H

#include <QObject>
#include <QDialog>
#include <QLabel>
#include <QLayout>
#include <QPushButton>

class M_Dialog : public QDialog
{
    Q_OBJECT
public:
    explicit M_Dialog(QString str /*сообщение, которое нужно вывести*/, QDialog *parent = 0);

signals:

public slots:
};

#endif // M_DIALOG_H
