#-------------------------------------------------
#
# Project created by QtCreator 2015-05-14T04:27:57
#
#-------------------------------------------------

QT       += core gui
CONFIG += console

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = maze_
TEMPLATE = app


SOURCES += main.cpp\
    Maze.cpp \
    Player.cpp \
    maze_widget.cpp \
    main_window.cpp \
    m_dialog.cpp

HEADERS  += \
    Maze.h \
    Player.h \
    maze_widget.h \
    main_window.h \
    m_dialog.h

QMAKE_CXXFLAGS += -std=gnu++11

FORMS +=

