#include "maze_widget.h"

Maze_Widget::Maze_Widget(int h, int w, QWidget *parent) : QWidget(parent)
{
    this->setFocusPolicy(Qt::StrongFocus);
    mzx = new Maze(h,w);
    mzx->Generate();
    pl = new Player(*mzx);
}

void Maze_Widget::pUp()
{
    if (pl->y > 0)
    {
        if (!mzx->Maze_[pl->y - 1][pl->x].d_wall)
        {
            mzx->Maze_[pl->y - 1][pl->x].player = true;
            mzx->Maze_[pl->y][pl->x].player = false;
            pl->y--;
        }
    }
}

void Maze_Widget::pDown()
{
    if (pl->y + 1 < mzx->Height)
    {
        if (!mzx->Maze_[pl->y][pl->x].d_wall)
        {
            mzx->Maze_[pl->y][pl->x].player = false;
            mzx->Maze_[pl->y + 1][pl->x].player = true;
            pl->y++;
        }
    }
}

void Maze_Widget::pLeft()
{
    if (pl->x - 1 > 0)
    {
        if (!mzx->Maze_[pl->y][pl->x - 1].r_wall)
        {
            mzx->Maze_[pl->y][pl->x - 1].player = true;
            mzx->Maze_[pl->y][pl->x].player = false;
            pl->x--;
        }
    }
}

void Maze_Widget::pRight()
{
    if (pl->x + 1 < mzx->Width)
    {
        if (!mzx->Maze_[pl->y][pl->x].r_wall)
        {
            mzx->Maze_[pl->y][pl->x].player = false;
            mzx->Maze_[pl->y][pl->x + 1].player = true;
            pl->x++;
        }
    }
}

void Maze_Widget::keyPressEvent(QKeyEvent *pressed)
{
    if (mzx->Maze_[pl->y][pl->x].player!=mzx->Maze_[pl->y][pl->x].out)
    {
        switch (pressed->key())
        {
        case Qt::Key_Up:
            pUp();
            break;
        case Qt::Key_Down:
            pDown();
            break;
        case Qt::Key_Left:
            pLeft();
            break;
        case Qt::Key_Right:
            pRight();
            break;
        }
        this->update();
    }
    else
    {
        M_Dialog *err = new M_Dialog(QString("Лабиринт пройден!"));
        err->show();
    }
}

void Maze_Widget::Generate_maze(int &w, int &h)
{
    delete mzx;
    delete pl;
    mzx = new Maze(h,w);
    mzx->Generate();
    pl = new Player(*mzx);
    this->setFocus();
    this->update();
}

void Maze_Widget::restart()
{
    mzx->Maze_[pl->y][pl->x].player = false;
    mzx->Maze_[mzx->ys][mzx->xs].player = true;
    pl->x=mzx->xs;
    pl->y=mzx->ys;
    this->setFocus();
    this->update();
}

void Maze_Widget::Bot()
{
    pl->Bot_Start(*mzx,pl->y,pl->x);
    this->setFocus();
    this->update();
}

void Maze_Widget::paintEvent(QPaintEvent *)
{
    QPainter p(this); // Создаём новый объект рисовальщика
    p.setPen(QPen(Qt::red,1,Qt::SolidLine));
    p.fillRect(0,0,(mzx->Width-1)*21,(mzx->Height-1)*21,Qt::darkGray);

    for (int i = 1; i < mzx->Height; i++)
    {
        for(int j = 1; j < mzx->Width; j++)
        {
            if (mzx->Maze_[i][j].player == true)
            {
                    if (mzx->Maze_[i][j].out == true)
                    {
                        p.fillRect((j-1)*21+0.5,(i-1)*21+0.5,20-1,20-1,Qt::darkRed);
                    }
                    else
                    p.fillRect((j-1)*21+0.5,(i-1)*21+0.5,20-1,20-1,Qt::green);
            }
            else
                if (mzx->Maze_[i][j].out == true)
                {
                    p.fillRect((j-1)*21+0.5,(i-1)*21+0.5,20-1,20-1,Qt::blue);
                }

            if (mzx->Maze_[i][j].r_wall == true)
            {
                p.drawLine((j-1)*21+20,(i-1)*21,(j-1)*21+20,(i-1)*21+20);
            }
            else
            {
                p.setPen(QPen(Qt::lightGray,0.2,Qt::DashLine));
                p.drawLine((j-1)*21+20,(i-1)*21,(j-1)*21+20,(i-1)*21+20);
                p.setPen(QPen(Qt::red,1,Qt::SolidLine));
            }
            if (mzx->Maze_[i][j].d_wall == true)
            {
                p.drawLine((j-1)*21,(i-1)*21+20,(j-1)*21+20,(i-1)*21+20);
            }
            else
            {
                p.setPen(QPen(Qt::lightGray,0.2,Qt::DashLine));
                p.drawLine((j-1)*21,(i-1)*21+20,(j-1)*21+20,(i-1)*21+20);
                p.setPen(QPen(Qt::red,1,Qt::SolidLine));
            }
        }
    }

    p.drawLine(0,0,(mzx->Width-1)*21-1,0);
    p.drawLine(0,0,0,(mzx->Height-1)*21-1);

    this->setGeometry(5,5, mzx->Width*21,mzx->Height*21);

    if (mzx->way.length() != 0)
    {
        p.setPen(QPen(Qt::green,0.7,Qt::SolidLine));
        int x = pl->x;
        int y = pl->y;
        for (int i = 0; i < int(mzx->way.length() - 1); i++)
        {
            switch (mzx->way[i])
            {
            case 'u':
                y--;
                break;
            case 'd':
                y++;
                break;
            case 'l':
                x--;
                break;
            case 'r':
                x++;
                break;
            }
            p.drawRect((x-1)*21+5,(y-1)*21+5,20-10,20-10);
        }
        mzx->way="\0";
        p.setPen(QPen(Qt::red,1,Qt::SolidLine));
    }
}
