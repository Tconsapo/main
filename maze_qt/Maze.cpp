#include "Maze.h"


Maze::Maze(const int &h, const int &w) //создание поля лабиринта
{
    Height = h + 1;
    Width = w + 1;
    Maze_ = new T*[Height];
    for (int i = 0; i < Height; i++)
    {
        Maze_[i] = new T[Width];
    }

    for (int i = 0; i < Height; i++) //заполнение крайних стен
    {
        Maze_[i][0].r_wall = true;
        Maze_[i][Width - 1].r_wall = true;
    }

    for (int i = 0; i < Width; i++)
    {
        Maze_[0][i].d_wall = true;
        Maze_[Height - 1][i].d_wall = true;
    }
}

Maze::Maze(const Maze &x)
{
    Height = x.Height;
    Width = x.Width;
    xf = x.xf;
    xs = x.xs;
    yf = x.yf;
    ys = x.ys;
    Maze_ = new T*[Height];
    for (int i = 0; i < Height; i++)
    {
        Maze_[i] = new T[Width];
    }
    for (int i = 0; i < x.Height; i++)
    {
        for (int j = 0; j < x.Width; j++)
        {
            Maze_[i][j].d_wall = x.Maze_[i][j].d_wall;
            Maze_[i][j].r_wall = x.Maze_[i][j].r_wall;
            Maze_[i][j].out = x.Maze_[i][j].out;
            Maze_[i][j].player = x.Maze_[i][j].player;
        }
    }
}

Maze Maze::operator = (const Maze &x)
{
    Height = x.Height;
    Width = x.Width;
    xf = x.xf;
    xs = x.xs;
    yf = x.yf;
    ys = x.ys;
    Maze_ = new T*[Height];
    for (int i = 0; i < Height; i++)
    {
        Maze_[i] = new T[Width];
    }
    for (int i = 0; i < x.Height; i++)
    {
        for (int j = 0; j < x.Width; j++)
        {
            Maze_[i][j].d_wall = x.Maze_[i][j].d_wall;
            Maze_[i][j].r_wall = x.Maze_[i][j].r_wall;
            Maze_[i][j].out = x.Maze_[i][j].out;
            Maze_[i][j].player = x.Maze_[i][j].player;
        }
    }
    return *this;
}

bool fnd(const int &y, const int &x, bool** b, const Maze &m) //есть ли непосещенные клетки рядом
{
    if (x + 1 < m.Width)
    {
        if (!b[y][x + 1])
        {
            return true;
        }
    }
    if (y + 1 < m.Height)
    {
        if (!b[y + 1][x])
        {
            return true;
        }
    }
    if (x - 1 > 0)
    {
        if (!b[y][x - 1])
        {
            return true;
        }
    }
    if (y - 1 > 0)
    {
        if (!b[y - 1][x])
        {
            return true;
        }
    }
    return false;
}

//1-up,2-down,3-left,4-right
void Way(const int &y, const int &x, bool** Be, Maze &a) //генерация
{
    int xx = x;
    int yy = y;

    while (fnd(yy, xx, Be, a))  //проверяем можно ли сходить в непосещенную клетку
    {							// пока если клетки есть
        bool f = true;
        while (f)
        {
            int w = rand() % 4 + 1; //выбираем случайную клетку
            switch (w)				//проверяем можно ли в нее пойти
            {						//если нет - выбираем другую
            case 1:
                if (yy - 1 > 0)
                {
                    if (!Be[yy - 1][xx])
                    {
                        Be[yy][xx] = true;
                        a.Maze_[yy - 1][xx].d_wall = false;
                        a.Maze_[yy - 1][xx].r_wall = true;
                        if (yy - 2 > 0)
                        {
                            a.Maze_[yy - 2][xx].d_wall = true;
                        }
                        if (xx - 1 > 0)
                        {
                            a.Maze_[yy - 1][xx - 1].r_wall = true;
                        }
                        yy = yy - 1;
                        f = false;
                    }
                }
                break;
            case 2:
                if (yy + 1 < a.Height)
                {
                    if (!Be[yy + 1][xx])
                    {
                        Be[yy][xx] = true;
                        a.Maze_[yy][xx].d_wall = false;
                        a.Maze_[yy + 1][xx].d_wall = true;
                        a.Maze_[yy + 1][xx].r_wall = true;
                        if (xx - 1 > 0)
                        {
                            a.Maze_[yy + 1][xx - 1].r_wall = true;
                        }
                        yy = yy + 1;
                        f = false;
                    }
                }
                break;
            case 3:
                if (xx - 1 > 0)
                {
                    if (!Be[yy][xx - 1])
                    {
                        Be[yy][xx] = true;
                        a.Maze_[yy][xx - 1].r_wall = false;
                        if (xx - 2 > 0)
                        {
                            a.Maze_[yy][xx - 2].r_wall = true;
                        }
                        if (yy - 1 > 0)
                        {
                            a.Maze_[yy - 1][xx - 1].d_wall = true;
                        }
                        a.Maze_[yy][xx - 1].d_wall = true;
                        xx = xx - 1;
                        f = false;
                    }
                }
                break;
            case 4:
                if (xx + 1 < a.Width)
                {
                    if (!Be[yy][xx + 1])
                    {
                        Be[yy][xx] = true;
                        a.Maze_[yy][xx].r_wall = false;
                        a.Maze_[yy][xx + 1].r_wall = true;
                        a.Maze_[yy][xx + 1].d_wall = true;
                        if (yy - 1 > 0)
                        {
                            a.Maze_[yy - 1][xx + 1].d_wall = true;
                        }
                        xx = xx + 1;
                        f = false;
                    }
                }
                break;
            default:
                break;
            }
        }
        Be[yy][xx] = true;
    }

    if ((a.Maze_[yy][xx].d_wall == true)		//если вокруг выбранной клетки стены
            && (a.Maze_[yy][xx].r_wall == true)		//убираем случайную стену
            && (a.Maze_[yy - 1][xx].d_wall == true)
            && (a.Maze_[yy][xx - 1].r_wall == true))
    {
        bool f = true;
        while (f)
        {
            int w = rand() % 4 + 1;
            switch (w)
            {
            case 1:
                if (yy - 1 > 0)
                {
                    a.Maze_[yy - 1][xx].d_wall = false;
                    f = false;
                }
                break;
            case 2:
                if (yy < a.Height - 1)
                {
                    a.Maze_[yy][xx].d_wall = false;
                    f = false;
                }
                break;
            case 3:
                if (xx - 1 > 0)
                {

                    a.Maze_[yy][xx - 1].r_wall = false;
                    f = false;
                }
                break;
            case 4:
                if (xx < a.Width - 1)
                {
                    a.Maze_[yy][xx].r_wall = false;
                    f = false;
                }
                break;
            default:
                break;
            }
        }
    }
}

bool fndb(bool** b, const int &y, const int &x, const int &ym, const int &xm) //есть ли рядом посещенные клетки
{
    if (b[y - 1][x]) return true;
    if (b[y][x - 1]) return true;
    if (y + 1 < ym)
    {
        if (b[y + 1][x]) return true;
    }
    if (x + 1 < xm)
    {
        if (b[y][x + 1]) return true;
    }
    return false;
}

void Maze::Generate()
{
    srand(time(0));
    int xx = 1;
    int yy = rand() % (Height - 1) + 1;
    int fx = Width - 1;
    int fy = rand() % (Height - 1) + 1;

    Maze &a = *this;

    a.xs = xx;
    a.ys = yy;
    a.Maze_[yy][xx].player = true; //ставим игрока на вход

    a.xf = fx;
    a.yf = fy;

    a.Maze_[fy][fx].out = true; //выход

    bool** Be;		//карта посещенных
    Be = new bool*[Height];
    for (int i = 0; i < Height; i++)
    {
        Be[i] = new bool[Width];
        for (int j = 0; j < Width; j++)
        {
            Be[i][j] = false;
        }
    }

    a.Maze_[yy][xx].r_wall = true; //стены воруг входа
    a.Maze_[yy][xx].d_wall = true;
    if (yy - 1>0)
    {
        a.Maze_[yy - 1][xx].d_wall = true;
    }
    if (xx - 1 > 0)
    {
        a.Maze_[yy][xx - 1].r_wall = true;
    }
    Way(yy, xx, Be, a); //генерация случайного пути "до тупика"
    bool p = true;
    while (p)
    {
        p = false;
        for (int i = 1; i < a.Height; i++)
        {
            for (int j = 1; j < a.Width; j++)
            {
                if (!Be[i][j])	//ищем непосещенную клетку
                {
                    p = true;
                    if (fndb(Be, i, j, a.Height, a.Width)) //рядом с посещенной
                    {
                        Be[i][j] = true; //"посещаем"
                        bool f = true;
                        while (f)
                        {
                            int w = rand() % 4 + 1;
                            switch (w) //соединяем клетки и запускаем новую генерацию пути
                            {
                            case 1:
                                if (Be[i - 1][j])
                                {
                                    a.Maze_[i][j].r_wall = true;
                                    a.Maze_[i][j].d_wall = true;
                                    if (i - 1 > 0)
                                    {
                                        a.Maze_[i - 1][j].d_wall = true;
                                    }
                                    if (j - 1 > 0)
                                    {
                                        a.Maze_[i][j - 1].r_wall = true;
                                    }
                                    a.Maze_[i - 1][j].d_wall = false;
                                    Way(i, j, Be, a);
                                    f = false;
                                }
                                break;
                            case 2:
                                if (i + 1 < a.Height)
                                {
                                    if (Be[i + 1][j])
                                    {
                                        a.Maze_[i][j].r_wall = true;
                                        a.Maze_[i][j].d_wall = true;
                                        if (i - 1 > 0)
                                        {
                                            a.Maze_[i - 1][j].d_wall = true;
                                        }
                                        if (j - 1 > 0)
                                        {
                                            a.Maze_[i][j - 1].r_wall = true;
                                        }
                                        a.Maze_[i][j].d_wall = false;
                                        Way(i, j, Be, a);
                                        f = false;
                                    }
                                }
                                break;
                            case 3:
                                if (Be[i][j - 1])
                                {
                                    a.Maze_[i][j].r_wall = true;
                                    a.Maze_[i][j].d_wall = true;
                                    if (i - 1 > 0)
                                    {
                                        a.Maze_[i - 1][j].d_wall = true;
                                    }
                                    if (j - 1 > 0)
                                    {
                                        a.Maze_[i][j - 1].r_wall = true;
                                    }
                                    a.Maze_[i][j - 1].r_wall = false;
                                    Way(i, j, Be, a);
                                    f = false;
                                }
                                break;
                            case 4:
                                if (j + 1< a.Width)
                                {
                                    if (Be[i][j + 1])
                                    {
                                        a.Maze_[i][j].r_wall = true;
                                        a.Maze_[i][j].d_wall = true;
                                        if (i - 1 > 0)
                                        {
                                            a.Maze_[i - 1][j].d_wall = true;
                                        }
                                        if (j - 1 > 0)
                                        {
                                            a.Maze_[i][j - 1].r_wall = true;
                                        }
                                        a.Maze_[i][j].r_wall = false;
                                        Way(i, j, Be, a);
                                        f = false;
                                    }
                                }
                                break;
                            default:
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    for (int i = 0; i < Height; i++)
        delete[] Be[i];
    delete[] Be;
}

std::ostream &operator << (std::ostream &out, const Maze &x)
{
    for (int i = 1; i < x.Width; i++)
    {
        out << "##";
    }
    out << "#";

    out << "	X - Игрок" << std::endl;

    for (int i = 1; i < x.Height; i++)
    {
        out << "#";
        for (int j = 1; j < x.Width; j++)
        {
            if (x.Maze_[i][j].player)
            {
                if (x.Maze_[i][j].r_wall) out << "X#";
                else out << "X ";
            }
            else if (x.Maze_[i][j].out)
            {
                if (x.Maze_[i][j].r_wall) out << "O#";
                else out << "O ";
            }
            else
            {
                if (x.Maze_[i][j].r_wall) out << " #";
                else out << "  ";
            }

        }
        if (i == 1) out << "	O - Выход";
        out << std::endl << "#";
        for (int j = 1; j < x.Width; j++)
        {
            if (x.Maze_[i][j].d_wall)
            {
                out << "##";
            }
            else
                if (x.Maze_[i][j].r_wall)
                {
                    out << " #";
                }
                else
                    if (i + 1 < x.Height)
                    {
                        if (x.Maze_[i + 1][j].r_wall)
                        {
                            out << " #";
                        }
                        else out << "  ";
                    }
                    else out << "  ";
        }
        out << std::endl;
    }
    return out;
}

Maze::~Maze()
{
    for (int i = 0; i < Height; i++)
        delete[] Maze_[i];
    delete[] Maze_;
}
