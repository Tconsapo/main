#include "m_dialog.h"

M_Dialog::M_Dialog(QString str, QDialog *parent) : QDialog(parent)
{
    QLabel *msg = new QLabel;
    msg->setText(str);

    QVBoxLayout *lay = new QVBoxLayout;
    lay->addWidget(msg);

    QPushButton *ext = new QPushButton("OK");
    connect(ext,SIGNAL(clicked(bool)),this,SLOT(close()));
    ext->setFixedSize(90,30);
    lay->addWidget(ext);
    ext->setFocus();

    lay->setAlignment(ext,Qt::AlignHCenter);
    lay->setAlignment(msg,Qt::AlignHCenter);

    this->setLayout(lay);
    this->setMinimumSize(200,75);
}

