//класс игрока

#ifndef PLAYER_H
#define PLAYER_H


#include <conio.h>
#include "Maze.h"
#include <string>

#define UP_ARROW 72
#define LEFT_ARROW 75
#define DOWN_ARROW 80
#define RIGHT_ARROW 77

class Player
{
    int y;
    int x;

public:
    friend class Maze;
    friend class Maze_Widget;
    void Bot_Start(class Maze &m, const int &start_y, const int &start_x);
    void Bot_Way(class Maze &m, const int &start_y, const int &start_x, bool** &be, std::string** &ways);
    friend void Bot_Start(class Maze &m, const int &start_y, const int &start_x);
    Player();
    Player(class Maze &m);
    std::string Get_way(class Maze &mh, const int &n);
    ~Player();
};


#endif // PLAYER_H
