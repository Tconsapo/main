//Класс лабиринта

#ifndef MAZE_H
#define MAZE_H

#include <iostream>
#include<random>
#include<time.h>
#include"Player.h"
#include<string>

struct T //клетка лабиринта
{
    bool d_wall = false;
    bool r_wall = false;
    bool out = false;
    bool player = false;
};

class Maze
{
    T** Maze_;  //Клетка лабиринта
    int Height; //Высота лабиринта
    int Width;  //Ширина лабиринта
    int xs, ys, xf, yf; //старт,финиш
    std::string way; //путь из текущей клетки

public:
    Maze(const int &h /*высота*/, const int &w /*ширина*/);
    friend class Player;
    friend class Maze_Widget;
    friend class Main_Window;
    friend class Maze_Console;
    void Generate(); //генерация
    friend std::ostream &operator << (std::ostream &out, const Maze &x);

    friend bool fnd(const int &y, const int &x/*позиция у, х*/, bool** b /*массив посещенных клеток*/,
                    const Maze &m /*лабиринт*/); //есть ли рядом непосещенные клетки

    friend void Way(const int &y, const int &x, bool** _Be /*массив посещенных*/, Maze &m /*лабиринт*/); //генерация
    friend void Bot_Way(const class Maze &m, const int &start_y, const int &start_x, bool** be,
                        std::string** ways); //путь бота
    friend void Bot_Start(class Maze &m, const int &start_y, const int &start_x); //старт бота
    Maze operator = (const Maze &x);
    Maze(const Maze &x);
    ~Maze();
};

#endif // MAZE_H
