//Главное окно

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include <QWidget>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTextEdit>
#include "maze_widget.h"
#include "m_dialog.h"

class Main_Window : public QWidget
{
    Q_OBJECT

    QPushButton *Generate;  //Создать
    QPushButton *Restart;   //Начать заного
    QPushButton *Get_Way;   //Путь
    QPushButton *ext;   //выход
    QTextEdit *height_e;    //поле высоты
    QTextEdit *width_e; //поле ширины
    Maze_Widget *mz; //Виджет лабиринта

    friend class Maze_Widget;

public:
    explicit Main_Window(QWidget *parent = 0);
protected:

signals:
    void Gen_ok(int& /*ширина*/,int& /*высота*/); //проверка условий
public slots:
    void Generate_mz(); //Генерация лабиринта
};

#endif // MAIN_WINDOW_H
