/*
 �� ��������� ����� ����� ������ ������ � ��� ����� ����� (�� ����������� � ���������).
 ����������, �� ��������� �� ������ ��� ����, ���� ���� ������, �� �� ���� ������. ���������� ������ �������.
*/

#include <iostream>;

using namespace std;

int N[4], c[4];

int find(int x)
{
	int i, j;
	i = c[x]+1;
	j = N[x]+1;
	while ((i <= 8) && (j <= 8))
	{
		if ((i == c[0]) && (j == N[0]))
		{
			return(1);
		}
		i++;
		j++;
	}
	
	i = c[x] - 1;
	j = N[x] - 1;
	while ((i > 0) && (j > 0))
	{
		if ((i == c[0]) && (j == N[0]))
		{
			return(1);
		}
		i--;
		j--;
	}

	i = c[x] - 1;
	j = N[x] + 1;
	while ((i > 0) && (j <= 8))
	{
		if ((i == c[0]) && (j == N[0]))
		{
			return(1);
		}
		i--;
		j++;
	}

	i = c[x] + 1;
	j = N[x] - 1;
	while ((i <= 8) && (j > 0))
	{
		if ((i == c[0]) && (j == N[0]))
		{
			return(1);
		}
		i++;
		j--;
	}
	return(0);
}

int main()
{
	setlocale(LC_ALL, "Russian");
	int i;
	char H;
	bool f = true;
	for (i = 0; i <= 3; i++)
	{
		cin >> H >> N[i];
		if (int(H) < 73)
			c[i] = int(H) - 64;
		else
			c[i] = int(H) - 96;
	}
	for (i = 1; i <= 3; i++)
	{
		if ((c[i] == c[0]) || (N[i] == N[0]) || (find(i)))
		{
			cout << "������ �� ����� �" << i << " � ������������: " << char(c[i] + 64) << ' ' << N[i] << endl;
			f = false;
		}
	}
	if (f)
	{
		cout << "������ ���" << endl;
	}
	return(0);
}