#include "pPoint.h"

using namespace std;

	pPoint::pPoint()
	{
		f = 0;
		r = 0;
	}

	pPoint::~pPoint()
	{
	}

	pPoint::pPoint(pPoint& x)
	{
		f = x.f;
		r = x.r;
	}

	pPoint pPoint::operator + (const pPoint& y) const
	{
		pPoint a;
		int f1;
		if (f > y.f)
		{
			f1 = y.f - f;
		}
		else
		{
			f1 = f - y.f;
		}
		a.r = sqrt(pow(r, 2) + pow(y.r, 2) - 2 * r*y.r*cos(PI*(180 + f1) / 180));
		double xx = r*cos(PI*(f) / 180) + y.r*cos(PI*(y.f) / 180);
		double yy = r*sin(PI*(f) / 180) + y.r*sin(PI*(y.f) / 180);
		if (xx>0) a.f = 180 * asin(yy / a.r) / PI + 0, 5;
		if (xx<0) a.f = 180 - 180 * asin(yy / a.r) / PI + 0, 5;
		if (a.f < 0)
			a.f = 360 + a.f;
		if (a.f>360)
			a.f = a.f - 360;
		if (a.f == 360)
			a.f = 0;
		return a;
	}

	pPoint pPoint::operator - (const pPoint& y)const
	{
		pPoint a;
		a = pPoint:: operator+ (-y);
		return a;
	}

	pPoint pPoint::operator - () const
	{
		pPoint a;
		if (f + 180 < 360)
		{
			a.f = f + 180;
		}
		else
		{
			a.f = f - 180;
		}
		a.r = r;
		return a;
	}

	pPoint pPoint::operator * (const int n)const
	{
		pPoint a;
		a.r = r*n;
		a.f = f;
		return a;
	}

	double pPoint::d_pp(const pPoint& y)const
	{
		return(sqrt(pow(r, 2) + pow(y.r, 2) - 2 * r*y.r*cos(PI*(abs(f - y.f)) / 180)));
	}

	dPoint pPoint::toDec() const
	{
		dPoint a;
		a.rx(r*cos(PI*(f) / 180));
		a.ry(r*sin(PI*(f) / 180));
		return a;
	}

	istream& operator >>(istream& in, pPoint& k)
	{
		in >> k.f >> k.r;
		return in;
	}

	ostream& operator <<(ostream& out, const pPoint& k)
	{
		out << k.f << " | " << k.r;
		return out;
	}