#include "dPoint.h"


dPoint::dPoint()
{
	x = 0;
	y = 0;
}


dPoint::~dPoint()
{
}

dPoint::dPoint(dPoint& k)
{
	x = k.x;
	y = k.y;
}

void dPoint::rx(double k)
{
	x = k;
}

void dPoint::ry(double k)
{
	y = k;
}

ostream& operator <<(ostream& out, dPoint k)
{
	if (int(k.x) == -0) 
		k.x = 0;	 
	if (int(k.y) == -0)
		k.y = 0;
	out << "(" << fixed << setprecision(2) << k.x << ";" << k.y << ")" << endl;
	return out;
}

istream& operator >>(istream& in, dPoint& k)
{
	in >> k.x >> k.y;
	return in;
}
