#include "pPoint.h"

using namespace std;

int main()
{					  
	pPoint a, b;
	int n;
	cin >> a >> b >> n;
	pPoint d = a;
	cout << a << " = " << d << endl;
	cout << "from a to b = " << a.d_pp(b) << endl;
	cout << "a + b = " << a+b << endl;
	cout << "Invert a = " << -a << endl;
	cout << "a - b = " << a-b << endl;
	cout << "a * n = " << a*n << endl;
	cout << "decart a = " << a.toDec()<<endl;
	system("pause");
	return 0;
}