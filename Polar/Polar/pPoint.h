#pragma once

#include <iostream>
#include "dPoint.h"


using namespace std;

const double PI = 3.14159265358979323846;

class pPoint
{
	int f;
	double r;
public:
	friend istream& operator >>(istream& in, pPoint& k);
	friend ostream& operator <<(ostream& out, const pPoint& k);	
	pPoint();
	~pPoint();
	pPoint(pPoint& x);
    pPoint operator + (const pPoint& y) const;
	pPoint operator - (const pPoint& y) const;
	pPoint operator -() const;
	pPoint operator * (const int n) const;
	double d_pp(const pPoint& y) const;
	dPoint toDec() const;
};