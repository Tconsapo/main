#pragma once
#include <iostream>
#include <iomanip>

using namespace std;

class dPoint
{						   
	double x, y;
public:
	dPoint();
	dPoint(dPoint& k);
	~dPoint();
	void rx(const double k);
	void ry(const double k);
	friend ostream& operator <<(ostream& out, dPoint k);
	friend istream& operator >>(istream& in, dPoint& k);
};