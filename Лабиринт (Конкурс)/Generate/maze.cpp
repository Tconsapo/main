#include "maze.h"

z::z()
{
	x = -1;
	y = -1;
}

z z::operator =(z &z0)
{
	int x = z0.x, y = z0.y;
	z r(x, y);
	return r;
}

z::z(int &xx, int &yy)
{
	x = xx;
	y = yy;
}

bool z::is_it(int &xx, int &yy)
{
	if ((x == xx) && (y == yy))
		return true;
	else
		return false;
}

void maze::push_back(int xx, int yy)
{
	z zz(xx, yy);
	Be.push_back(zz);
}

maze::maze()
{
	val = 0;
	bonus = 0;
	wall = false;
	was = false;
	way = "\0";
}

maze::maze(List &_Be, ListIterator &_ItBe, double &v, double &b, bool &y, bool &f, std::string &w)
{
	Be = _Be;
	ItBe = _ItBe;
	val = v;
	bonus = b;
	wall = y;
	was = f;
	way = w;
}

maze::maze(double &v)
{
	val = v;
	bonus = 0;
	wall = false;
	was = false;
	way = "\0";
}

maze::maze(bool &y)
{
	val = 0;
	bonus = 0;
	wall = y;
	was = false;
	way = "\0";
}

maze::maze(maze &x)
{
	if (!x.Be.empty())
	{
		x.ItBe = x.Be.begin();
		while (x.ItBe != x.Be.end())
		{										 
			z z1 = *x.ItBe;
			push_back(z1.x, z1.y);
			advance(x.ItBe, 1);
		}
	}
	ItBe = x.ItBe;
	wall = x.wall;
	bonus = x.bonus;
	val = x.val;
	was = x.was;
	way = x.way;
}

maze::~maze()
{
}
