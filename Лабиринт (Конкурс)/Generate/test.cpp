#include <iostream>
#include <fstream>
#include <limits>
#include <time.h>
#include "maze.h"

using namespace std;

int y_l = 0; // ������� ���������
int x_l = 0;
time_t ti;
maze best; //������ �� ������

void do_find(int &x, int &y, int &fx, int &fy, maze** k) //����� ����
{
	if ( time(NULL) - ti >= 60)
	{
		cout << best.way << endl;
		system("pause");
		exit(0);
	}
	if ((x != fx) || (y != fy))
	{

		k[fx][fy] = best;
		maze** k0 = new maze*[y_l];
		for (int i = 0; i < y_l; i++)
		{
			k0[i] = new maze[x_l];
			for (int j = 0; j < x_l; j++)
				k0[i][j] = k[i][j];
		} //����� ��������� �� ������ �����, ��� ��������� ����������� ���� � �����(�� �������� ����)

		//_______________________________________________________________���� �� ���� � ����� �����

		if ((k[x][y + 1].was == false) && (k[x][y + 1].wall == false)) //������
		{
			k[x][y + 1].val = k[x][y + 1].bonus + k[x][y].val - 0.1; //��������� �������� � �����
			k[x][y + 1].way = k[x][y].way + "r"; //����������� �������� ���� � �����
			k[x][y + 1].was = true; //���� � �����
			k[x][y + 1].Be = k[x][y].Be; //���������� ���������� �������
			if (k[x][y + 1].bonus != 0) k[x][y + 1].push_back(x, y + 1); //���� ������ � ����� �� 0 ��������� ��� � ����� �������
			int yy = y + 1;

			do_find(x, yy, fx, fy, k); //��������� ���
			
			if (k[fx][fy].val < best.val)  //��������� � ������ �� ������ ����
			{
				k[fx][fy].val = best.val;
				k[fx][fy].way = best.way;
				k[fx][fy].Be = best.Be;
			}
			else
			{
				best.val = k[fx][fy].val;
				best.way = k[fx][fy].way;
				best.Be = k[fx][fy].Be;
			}
	
			k = k0; //����� ����������� ��� �����
			k[fx][fy] = best; //���������� ������� ����������
		}

		if ((k[x + 1][y].was == false) && (k[x + 1][y].wall == false)) //����
		{
			k[x + 1][y].val = k[x + 1][y].bonus + k[x][y].val - 0.1;
			k[x + 1][y].way = k[x][y].way + "d";
			k[x + 1][y].was = true;
			k[x + 1][y].Be = k[x][y].Be;
			if (k[x + 1][y].bonus != 0) k[x + 1][y].push_back(x + 1, y);
			int xx = x + 1;

			do_find(xx, y, fx, fy, k);
			
			if (k[fx][fy].val < best.val)
			{
				k[fx][fy].val = best.val;
				k[fx][fy].way = best.way;
				k[fx][fy].Be = best.Be;
			}
			else
			{
				best.val = k[fx][fy].val;
				best.way = k[fx][fy].way;
				best.Be = k[fx][fy].Be;
			}
	
			k = k0;
			k[fx][fy] = best;
		}

		if ((k[x][y - 1].was == false) && (k[x][y - 1].wall == false)) //�����
		{
			k[x][y - 1].val = k[x][y - 1].bonus + k[x][y].val - 0.1;
			k[x][y - 1].way = k[x][y].way + "l";
			k[x][y - 1].was = true;
			k[x][y - 1].Be = k[x][y].Be;
			if (k[x][y - 1].bonus != 0) k[x][y - 1].push_back(x, y - 1);
			int yy = y - 1;

			do_find(x, yy, fx, fy, k);
			
			if (k[fx][fy].val < best.val)
			{
				k[fx][fy].val = best.val;
				k[fx][fy].way = best.way;
				k[fx][fy].Be = best.Be;
			}
			else
			{
				best.val = k[fx][fy].val;
				best.way = k[fx][fy].way;
				best.Be = k[fx][fy].Be;
			}
	
			k = k0;
			k[fx][fy] = best;
		}
		if ((k[x - 1][y].was == false) && (k[x - 1][y].wall == false)) //�����
		{
			if (k[x][y].bonus != 0) k[x][y].push_back(x, y);
			k[x - 1][y].val = k[x - 1][y].bonus + k[x][y].val - 0.1;
			k[x - 1][y].way = k[x][y].way + "u";
			k[x - 1][y].was = true;
			k[x - 1][y].Be = k[x][y].Be;
			if (k[x - 1][y].bonus != 0) k[x - 1][y].push_back(x - 1, y);
			int xx = x - 1;

			do_find(xx, y, fx, fy, k);
			
			if (k[fx][fy].val < best.val)
			{
				k[fx][fy].val = best.val;
				k[fx][fy].way = best.way;
				k[fx][fy].Be = best.Be;
			}
			else
			{
				best.val = k[fx][fy].val;
				best.way = k[fx][fy].way;
				best.Be = k[fx][fy].Be;
			}

			k = k0;
			k[fx][fy] = best;
		}


		//_____________________________________________________________���� ����

		if ((k[x][y + 1].was == true) && (k[x][y + 1].wall == false)) //������
		{
			double b_val = k[x][y + 1].bonus; //��������� �������� ������
			
			if (k[x][y + 1].bonus != 0)					 //
			{											 //
				bool f = false;							 //
				k[x][y].ItBe = k[x][y].Be.begin();	 	 //
				if (!k[x][y].Be.empty())			 	 //
				while (k[x][y].ItBe != k[x][y].Be.end()) //��������� ���� �� �� ���� ����,
				{										 //����� �� ����� ����� ��������
														 //
					z z1 = *k[x][y].ItBe;				 //
					int yy = y + 1;						 //
					if (z1.is_it(x, yy))				 //
					{									 //
						f = true;						 //
						break;							 //
					}									 //
					advance(k[x][y].ItBe, 1);			 //
				};										 //
				if (f) k[x][y + 1].bonus = 0; //�������� �����, ���� ����
			}

			if (k[x][y].val - 0.1 + k[x][y + 1].bonus > k[x][y + 1].val)	//���� ���������� ����� ��� 
			{																//���� ���� � ����� ������ ����������
				k[x][y + 1].val = k[x][y].val - 0.1 + k[x][y + 1].bonus;
				k[x][y + 1].way = k[x][y].way + "r";
				k[x][y + 1].Be = k[x][y].Be;
				int yy = y + 1;

				do_find(x, yy, fx, fy, k);
				
				if (k[fx][fy].val < best.val)
				{
					k[fx][fy].val = best.val;
					k[fx][fy].way = best.way;
					k[fx][fy].Be = best.Be;
				}
				else
				{
					best.val = k[fx][fy].val;
					best.way = k[fx][fy].way;
					best.Be = k[fx][fy].Be;
				}
				
				k = k0;
				k[fx][fy] = best;

				k[x][y + 1].bonus = b_val; //���������� �����
			}
			else k[x][y + 1].bonus = b_val; //���������� �����
		}

		if ((k[x + 1][y].was == true) && (k[x + 1][y].wall == false)) //����
		{
			double b_val = k[x + 1][y].bonus;

			if (k[x + 1][y].bonus != 0)
			{
				bool f = false;
				k[x][y].ItBe = k[x][y].Be.begin();
				if (!k[x][y].Be.empty())
				while (k[x][y].ItBe != k[x][y].Be.end()) 
				{										 
					z z1 = *k[x][y].ItBe;
					int xx = x + 1;
					if (z1.is_it(xx, y))
					{
						f = true;
						break;
					}
					advance(k[x][y].ItBe, 1);;
				};
				if (f) k[x + 1][y].bonus = 0;
			}

			if (k[x][y].val - 0.1 + k[x + 1][y].bonus > k[x + 1][y].val)
			{
				k[x + 1][y].val = k[x][y].val - 0.1 + k[x + 1][y].bonus;
				k[x + 1][y].way = k[x][y].way + "d";
				int xx = x + 1;
				k[x + 1][y].Be = k[x][y].Be;
				
				do_find(xx, y, fx, fy, k);
				
				if (k[fx][fy].val < best.val)
				{
					k[fx][fy].val = best.val;
					k[fx][fy].way = best.way;
					k[fx][fy].Be = best.Be;
				}
				else
				{
					best.val = k[fx][fy].val;
					best.way = k[fx][fy].way;
					best.Be = k[fx][fy].Be;
				}

				k = k0;
				k[fx][fy] = best;

				k[x + 1][y].bonus = b_val; 
			}
			else k[x + 1][y].bonus = b_val;
		}

		if ((k[x - 1][y].was == true) && (k[x - 1][y].wall == false)) //�����
		{
			double b_val = k[x - 1][y].bonus; 

			if (k[x - 1][y].bonus != 0)
			{
				bool f = false;
				k[x][y].ItBe = k[x][y].Be.begin();
				if (!k[x][y].Be.empty())
				while (k[x][y].ItBe != k[x][y].Be.end()) 
				{										 
					z z1 = *k[x][y].ItBe;
					int xx = x - 1;
					if (z1.is_it(xx, y))
					{
						f = true;
						break;
					}
					advance(k[x][y].ItBe, 1);;
				};
				if (f) k[x - 1][y].bonus = 0; 
			}

			if (k[x][y].val - 0.1 + k[x - 1][y].bonus > k[x - 1][y].val)
			{
				k[x - 1][y].val = k[x][y].val - 0.1 + k[x - 1][y].bonus;
				k[x - 1][y].way = k[x][y].way + "u";
				int xx = x - 1;
				k[x - 1][y].Be = k[x][y].Be;
				
				do_find(xx, y, fx, fy, k);
				
				if (k[fx][fy].val < best.val)
				{
					k[fx][fy].val = best.val;
					k[fx][fy].way = best.way;
					k[fx][fy].Be = best.Be;
				}
				else
				{
					best.val = k[fx][fy].val;
					best.way = k[fx][fy].way;
					best.Be = k[fx][fy].Be;
				}
	
				k = k0;
				k[fx][fy] = best;

				k[x - 1][y].bonus = b_val; 
			}
			else k[x - 1][y].bonus = b_val;
		}

		if ((k[x][y - 1].was == true) && (k[x][y - 1].wall == false)) //�����
		{
			double b_val = k[x][y - 1].bonus;

			if (k[x][y - 1].bonus != 0)
			{
				bool f = false;
				k[x][y].ItBe = k[x][y].Be.begin();
				if (!k[x][y].Be.empty())
				while (k[x][y].ItBe != k[x][y].Be.end()) 
				{										 
					z z1 = *k[x][y].ItBe;
					int yy = y - 1;
					if (z1.is_it(x, yy))
					{
						f = true;
						break;
					}
					advance(k[x][y].ItBe, 1);;
				};
				if (f) k[x][y - 1].bonus = 0;
			}

			if (k[x][y].val - 0.1 + k[x][y - 1].bonus > k[x][y - 1].val)
			{
				k[x][y - 1].val = k[x][y].val - 0.1 + k[x][y - 1].bonus;
				k[x][y - 1].way = k[x][y].way + "l";
				int yy = y - 1;
				k[x][y - 1].Be = k[x][y].Be;

				do_find(x, yy, fx, fy, k);
				
				if (k[fx][fy].val < best.val) 
				{
					k[fx][fy].val = best.val;
					k[fx][fy].way = best.way;
					k[fx][fy].Be = best.Be;
				}
				else
				{
					best.val = k[fx][fy].val;
					best.way = k[fx][fy].way;
					best.Be = k[fx][fy].Be;
				}

				k = k0;
				k[fx][fy] = best;

				k[x][y - 1].bonus = b_val; 
			}
			else k[x][y - 1].bonus = b_val; 
		}

		for (int i = 0; i < y_l; i++)
		{
			delete[] k0[i];
		}
	}
}

int main()
{
	setlocale(LC_ALL, "Russian");

	string ifname;	//������� ����
	//cout << "������� ��� �������� �����: ";
	//cin >> ifname;
	//cout << endl;
	ifname = "in.txt";
	ifstream in(ifname);

	if (!in.is_open()) //�������� ��������
	{
		cout << "������ �������� �������� �����";
		system("pause");
		return 1;
	};

	ti = time(NULL);

	list<int> xl_m;
	list<int>::iterator ixl = xl_m.begin();

	string xl;
	//in >> xl;
	//x_l = xl.length(); //���������� ��������
	while (!in.fail()) //���������� �����
	{	
		getline(in, xl);
		int n = xl.length();
		xl_m.push_back(n);
		if (n > x_l) x_l = n;
		y_l++;
	};
	y_l--;
	//ixl = xl_m.begin();

	maze** a = new maze*[y_l]; //�������� ������� ���������
	for (int i = 0; i < y_l; i++)
	{
		a[i] = new maze [x_l];
	};

	in.clear();
	in.seekg(ios::beg); //���� ���������
	
	char h;
	int start_x, start_y, fin_x, fin_y; //��������� �� ����� � �����
	
	best.val = -DBL_MAX; // ��������� ������ ��������

	start_x = start_y = fin_x = fin_y = -1;	//��� �������� ������� ��� ����������

	ixl = xl_m.begin();
	for (int i = 0; i < y_l; i++)	//���������� ����� ���������
	{
		for (int j = 0; j < *ixl; j++)
		{
			in.get(h);
			if (h == '#') a[i][j].wall = true;	//�����
			else
			if (h == ' ') a[i][j].val = 0;		//������
			else
			if ((h >= '0') && (h <= '9'))		//���� �����
			{	
				a[i][j].bonus = int(h) - 48;
				/*in.get(h);
				while ((h >= '0') && (h <= '9')) //���� ����� > 9
				{
					a[i][j].bonus = a[i][j].val * 10 + int(h)-48;
					in.get(h);
				};
				int p = in.tellg();
				in.seekg(p - 1); //����� ��������� �����*/
			}
			else
			if (h == '*')						//�����
			{	
				if ((start_x != -1) || (start_y != -1))	//�������� ���������� �������
				{
					cout << "� ��������� ����� ������ ������!" << endl;
					system("pause");
					return 2;
				};
				a[i][j].was = true;
				start_x = i;
				start_y = j;
			}
			else
			if (h == 'e')						//�����
			{
				if ((fin_x != -1) || (fin_y != -1))	//�������� ���������� �������
				{
					cout << "� ��������� ����� ������ ������!" << endl;
					system("pause");
					return 2;
				};
				a[i][j].bonus = 0;
				fin_x = i;
				fin_y = j;
			}
			else //���� �������
			{
				cout << "� ����� ��������� ���������� ������������ ������!" << endl;
				system("pause");
				return 2;
			};
		};
		getline(in, xl);
		ixl++;
	};

	do_find(start_x, start_y, fin_x, fin_y, a);

	cout << best.way << endl;
	system("pause");
	
	for (int i = 0; i < y_l; i++)
	{
		delete[] a[i];
	}

	return 0;
}